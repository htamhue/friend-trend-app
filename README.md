# Friend Trend App

Mobile App

- Các flow, bố cục, các nút như app kia
- Giao diện thì theo design mới.
- Support đưa lên store.

Giai đoạn đầu sẽ build app sử dụng framework React Native version mới nhất, ngôn ngữ JavaScript, build both iOS và Android.
Ngôn ngữ tiếng anh, có hỗ trợ Push notification, hỗ trợ phone và tablet, chế độ portrait, tất cả thông tin trên app đều lấy từ backend, lấy dữ liệu realtime, nếu backend không hỗ trợ sẽ có 1 interval để fetch data. 
Về phần chart sẽ embed tradingview, và custom sao cho match UI nhất có thể (có thể k match 100%)
Giai đoạn này sẽ tập trung build những chức năng như mô tả:
1. Màn hình home: gồm có chart, indicator, khung giờ, thanh pivot, click notification icon để setting có thể enable/disable nhận thông báo từ backend
2. Show chart detail khi click vào view chart của 1 cặp tiền nào đó (ở đó có 3 chart như anh đã gửi trong group skype), indicator trên chart
3. My account: 
    1. Register, login, change pass
    2. view user detail, update, delete
    3. Subscribe sẽ call API tới backend, rồi lưu thông tin từng user chứ k có sử dụng IAP.

Về phần tính toán hiển thị chart theo EMA21, RSI, STOCHASTIC, hay là công thức giờ cần sử dụng trong app, sẽ tìm hiểu nhưng cũng nhờ anh cung cấp và giải thích dựa trên dữ liệu trả về từ backend.
